import World from './world.js';

((c) => {

  new World(c).start();

})(document.getElementById("app"));
