import Point from './point.js';

export default class Size {
  constructor(width, height, rotation) {
    this.width = width || 0;
    this.height = height || 0;
    this.rotation = rotation || 0;
    this.center = new Point(this.width * 0.5, this.height * 0.5);
  }
}
