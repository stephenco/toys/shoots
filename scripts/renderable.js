import Point from "./point.js";
import Vector from "./vector.js";
import Size from "./size.js";

export default class Renderable {
  constructor(position, size, vector) {
    this.position = position || new Point();
    this.size = size || new Size();
    this.vector = vector || new Vector();
    this.isStopped = false;
    this.isAlive = true;
    this.isGarbage = false;
  }

  collidesWith(obj) {
    var rect1 = { x: this.position.x - this.size.center.x, y: this.position.y - this.size.center.y, width: this.size.width, height: this.size.height };
    var rect2 = { x: obj.position.x - obj.size.center.x, y: obj.position.y - obj.size.center.y, width: obj.size.width, height: obj.size.height };

    if (rect1.x + rect1.width < rect2.x) { return false; }
    if (rect1.y + rect1.height < rect2.y) { return false; }
    if (rect1.x > rect2.x + rect2.width) { return false; }
    if (rect1.y > rect2.y + rect2.height) { return false; }

    return true;
  }

  start() {
    if (this.isAlive) {
      this.isStopped = false;
    }
    return this;
  }

  stop() {
    this.isStopped = true;
    return this;
  }

  remove() {
    this.isAlive = false;
    this.isGarbage = true;
    return this;
  }

  die(zombie) {
    this.stop();
    if (!zombie || !this.isAlive) {
      this.isGarbage = true;
    }
    this.isAlive = false;
    return this;
  }

  move() {
    if (this.isAlive && !this.isStopped) {
      this.position.x += this.vector.x;
      this.position.y += this.vector.y;
      this.size.rotation += this.vector.rotation;
    }
    return this;
  }

  draw(canvas) {
    if (this.isAlive && this.beforeDraw(canvas)) {
      this.paintShape(this.setupPaintContext(canvas));
    }
    return this;
  }

  beforeDraw(canvas) {
    return true;
  }

  setupPaintContext(canvas) {
    const context = canvas.getContext('2d');
    context.fillStyle = "#789";
    return context;
  }

  paintShape(context) {
    context.fillRect(
      this.position.x - this.size.center.x,
      this.position.y - this.size.center.y,
      this.size.width,
      this.size.height
    );
  }
}
