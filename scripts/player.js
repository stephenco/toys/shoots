import Renderable from './renderable.js';
import Point from './point.js';
import Size from './size.js';

export default class Player extends Renderable {
  constructor(canvas) {
    super(new Point(canvas.width * 0.5, canvas.height - 50), new Size(25, 25));
  }

  setupPaintContext(canvas) {
    const context = canvas.getContext('2d');
    context.fillStyle = '#fff';
    context.strokeStyle = '#111';
    context.lineWidth = 2;
    return context;
  }

  paintShape(context) {
    const halfWidth = this.size.width * 0.5;

    context.beginPath();
    context.moveTo(this.position.x, this.position.y - this.size.height);
    context.lineTo(this.position.x + halfWidth, this.position.y + this.size.height);
    context.lineTo(this.position.x - halfWidth, this.position.y + this.size.height);
    context.lineTo(this.position.x, this.position.y - this.size.height);
    context.closePath();

    context.fill();
    context.stroke();
  }
}
