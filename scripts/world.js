import Renderable from './renderable.js';
import Point from './point.js';
import Vector from './vector.js';
import Size from './size.js';
import Stars from './stars.js';
import Bullet from './bullet.js';
import Player from './player.js';
import Enemy from './enemy.js';
import EnemyBullet from './enemy-bullet.js';

export default class World {
  constructor(canvas) {
    this.canvas = canvas;
    this.audio = new (window.AudioContext || window.webkitAudioContext)();
    this.audioDestination = (function(ac) {
      const g = ac.createGain();
      g.gain.value = 0.05;
      g.connect(ac.destination);
      return g;
    })(this.audio);

    this.mousePos = new Point(-1, -1);
    this.layers = {
      background: [ new Stars(canvas) ],
      players: [ new Player(canvas) ],
    };

    const self = this;
    const shoot = () => {
      const player = self.layers.players[0];
      if (!this.gameOver && player) {

        self.beep(1000, 0.03);

        const pos = player.position;
        self.layers.projectiles.push(new Bullet(
          new Point(pos.x, pos.y),
          player
        ));
      }
    };

    canvas.addEventListener('click', shoot);
    window.addEventListener('keypress', e => {
      switch(e.charCode) {
        case 32: // spacebar
          shoot();
          break;

        case 112: // p
          self.togglePause();
          break;

        default:
          console.log(e);
          break;
      }
    });
    window.addEventListener('keydown', e => {
      const speed  = 2;
      const player = self.layers.players[0];
      if (!player) { return; }

      switch(e.keyCode) {
        case 37: // left
          self.mousePos = new Point(-1, -1);
          player.vector.x = Math.min(speed * -1, player.vector.x - speed);
          break;

        case 39: // right
          self.mousePos = new Point(-1, -1);
          player.vector.x = Math.max(speed, player.vector.x + speed);
          break;

        case 38: // up
          break;

        case 40: // down
          break;
      }
    });

    canvas.addEventListener('mousemove', e => {
      const rect = canvas.getBoundingClientRect();
      self.mousePos = new Point(e.clientX - rect.left, e.clientY - rect.top);
    });

    this.reset();
  }

  beep(freq, time, start) {
    const audio = this.audio;
    const osc = audio.createOscillator();
    osc.type = 'square';
    osc.frequency.value = freq || 440;
    osc.start(start || 0);
    osc.stop(audio.currentTime + (time || 0.1));
    osc.connect(this.audioDestination);
    return this;
  }

  reset() {
    this.points = 0;
    this.gameOver = false;
    this.layers.players = [ new Player(this.canvas) ];
    this.start();
    return this;
  }

  handleGameOver() {
    this.gameOver = true;
    this.beep(120, 0.1);
    this.beep(100, 0.2);
    this.beep(80, 0.3);
    this.beep(60, 0.5);
    console.log("GAME OVER! Final score:", this.points);
    console.log("Resetting in 6 seconds.");
    const self = this;
    setTimeout(() => self.reset(), 6000);
    return this;
  }

  addPoints(val) {
    if (!this.gameOver) {
      this.points = Math.max(0, this.points + val);
      console.log("Points:", this.points);
    }
    return this;
  }

  move() {
    const mouseX = this.mousePos.x;
    const player = this.layers.players[0];

    // check for game over
    if (!player && !this.gameOver) {
      return this.handleGameOver();
    }

    // check for player movement
    if (mouseX > 0 && !this.gameOver) {
      if (mouseX < player.position.x) {
        player.vector = new Vector().initGrid((player.position.x - mouseX) * - 0.05);
      } else if (mouseX > player.position.x) {
        player.vector = new Vector().initGrid((mouseX - player.position.x) * 0.05);
      } else {
        player.vector = new Vector();
      }
    }

    // function to enforce playable area
    const enforceBounds = obj => {
      if (obj.position.x < 0 || obj.position.x > this.canvas.width ||
        obj.position.y < 0 || obj.position.y > this.canvas.height) {
        obj.remove();

        if (obj.owner && obj.owner == player) {
          this.addPoints(-10);
        }

        if (obj instanceof Enemy) {
          this.addPoints(-70);
        }

        return true;

      }
      return false;
    };

    // move projectiles first
    const projectiles = this.layers.projectiles || [];
    for (let proj of projectiles) { enforceBounds(proj.move()); }

    // process renderable object movement
    for (let layer of [ "players", "enemies", "background", "foreground", "controls" ]) {
      for (let obj of this.layers[layer] || []) {
        if (!enforceBounds(obj.move()) && (layer == "enemies" || layer == "players")) {

          // check projectile collisions
          for (let proj of projectiles) {
            if (proj instanceof EnemyBullet && obj instanceof Enemy) { continue; }
            if (proj.collidesWith(obj)) {
              obj.die();
              proj.remove();
            }
          }

          // check for player collisions
          if (layer == "enemies" && player) {
            if (player.collidesWith(obj)) {
              player.die();
              obj.remove();
            }
          }

        }
      }
    }

    return this;
  }

  draw() {
    const c = this.canvas.getContext('2d');
    c.fillStyle = '#123';
    c.fillRect(0, 0, this.canvas.width, this.canvas.height);

    for (let layer of [ "background", "foreground", "projectiles", "players", "enemies", "controls" ]) {
      const cleanup = [];

      for (let obj of this.layers[layer] || []) {
        obj.draw(this.canvas);
        if (obj.isAlive) {
          cleanup.push(obj);
        }
      }

      delete this.layers[layer];
      this.layers[layer] = cleanup;
    }

    return this;
  }

  enemyCallback(enemy, reason, payload) {
    switch (reason) {

      case 'died':
        if (!this.gameOver) {
          this.beep(100, 0.1);
          this.beep(80, 0.3);
          this.addPoints(700);
        }
        break;

      case 'shot':
        if (!this.gameOver) {
          this.beep(240, 0.1);
          this.layers.projectiles = this.layers.projectiles || [];
          this.layers.projectiles.push(payload);
        }
        break;

      default:
        console.warn('Unexpected enemy callback reason: ', reason);
        break;

    }
  }

  spawnEnemy() {
    const self = this;
    const fn = () => {
      self.layers.enemies.push(new Enemy(this.canvas, (...args) => self.enemyCallback(...args)));
      clearTimeout(self.spawnTimeout);
      self.spawnTimeout = null;
    };

    if (!this.spawnTimeout) {
      this.spawnTimeout = setTimeout(fn, Math.random() * 2000);
    }

    return this;
  }

  start() {
    if (!this.started) {
      console.log('World started');
      this.started = true;
      this.motionSpeed = 30;

      const self = this;
      (function animationCallback() {
        if (self.started) {
          if ((self.layers.enemies || []).length < 25) {
            self.spawnEnemy();
          }
          self.move();
          self.draw();
          window.requestAnimationFrame(animationCallback);
        }
      })();
    }
    return this;
  }

  stop() {
    console.log('World stopped');
    this.started = false;
    if (this.spawnTimeout) {
      clearTimeout(this.spawnTimeout);
      this.spawnTimeout = null;
    }

    console.log(this.layers);

    return this;
  }

  togglePause() {
    return this.started ? this.stop() : this.start();
  }
}
