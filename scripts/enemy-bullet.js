import Bullet from './bullet.js';
import Vector from './vector.js';
import Size from './size.js';
import Point from './point.js';

export default class EnemyBullet extends Bullet {
  constructor(position, owner) {
    super(position, owner);
    this.size = new Size(6, 12);
    this.position = new Point(owner.position.x, owner.position.y);
    this.vector.initGrid(Math.random(), (Math.random() * 2) + 5);
  }

  setupPaintContext(canvas) {
    const context = canvas.getContext('2d');
    context.fillStyle = '#f60';
    return context;
  }
}
