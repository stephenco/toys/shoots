import Point from './point.js';
import Size from './size.js';
import Vector from './vector.js';
import Renderable from './renderable.js';

class Star extends Renderable {
  constructor(canvas, center) {
    super(new Point(center.x, center.y),
      new Size(1, 1),
      new Vector(Math.random() * 360, Math.random())
    );
    this.centerHeight = center.y;
    this.fullDist = canvas.height - center.y;
  }

  beforeDraw(canvas) {
    if (this.position.y != this.centerHeight) {
      const currentDist = Math.abs(this.position.y - this.centerHeight);
      this.scale = currentDist / this.fullDist;
      const size = 10 * this.scale;

      // TODO: calculate angular distance for a less 2d tunnel effect

      this.size.width = size;
      this.size.height = size;

      return true;
    }
    return false;
  }

  setupPaintContext(canvas) {
    const context = canvas.getContext('2d');
    // const sub = 150 * this.scale;
    // const val = 255 - sub;
    // context.fillStyle = "rgb(" + val + ", " + val + ", " + val + ")";
    context.fillStyle = '#457'; //'#78a';
    return context;
  }
}

export default class Stars extends Renderable {
  constructor(canvas) {
    super(
      new Point(canvas.width * 0.5, canvas.height * 0.5),
      new Size(canvas.width, canvas.height)
    );
    this.canvas = canvas;
    this.field = [];
    this.minStars = 3000;
  }

  move() {
    const speedMultiplier = 1.02;
    const stars = [];

    for (let star of this.field) {

      star.vector.setGrid(
        star.vector.x * speedMultiplier,
        star.vector.y * speedMultiplier
      );

      star.move();

      if (this.checkOutOfBounds(star)) {
        star.die();
      }

      if (!star.isGarbage) {
        stars.push(star);
      }
    }

    const center =  { x: this.canvas.width * 0.5, y: this.canvas.height * 0.3 }

    while (stars.length < this.minStars) {
      stars.push(new Star(this.canvas, center)); // TODO: why can't use this.center?
    }

    this.field = stars;
    return this;
  }

  draw() {
    for (let star of this.field) {
      star.draw(this.canvas);
    }
    return this;
  }

  checkOutOfBounds(obj) {
    const p = obj.position;
    const s = this.size;
    return p.x < 0 || p.y < 0 || p.x > s.width || p.y > s.height;
  }
}
