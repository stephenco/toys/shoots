import Renderable from './renderable.js';
import Point from './point.js';
import Size from './size.js';
import Vector from './vector.js';
import EnemyBullet from './enemy-bullet.js';

export default class Enemy extends Renderable {
  constructor(canvas, callback) {
    super(
      new Point(Math.random() * canvas.width, 0),
      new Size(50, 50),
      new Vector().initGrid(Math.random(), Math.random() * 5)
    );
    this.callback = callback || (() => {});
  }

  setupPaintContext(canvas) {
    const context = canvas.getContext('2d');
    context.fillStyle = '#29d';
    return context;
  }

  paintShape(context) {
    const size = Math.max(this.size.width, this.size.height);
    const halfSize = size * 0.5;
    context.beginPath();
    context.arc(this.position.x, this.position.y, halfSize, Math.PI * 2, false);
    context.fill();
    context.stroke();
  }

  spawnBullet() {
    const self = this;
    const fn = () => {
      this.callback(this, "shot", new EnemyBullet(this.position, this));
      clearTimeout(self.spawnTimeout);
      self.spawnTimeout = null;
    };

    if (!this.spawnTimeout) {
      this.spawnTimeout = setTimeout(fn, Math.random() * 2000);
    }

    return this;
  }

  move() {
    if (!this.isStopped) {
      this.spawnBullet();
      super.move();
    }
    return this;
  }

  remove() {
    if (this.spawnTimeout) {
      clearTimeout(this.spawnTimeout);
      this.spawnTimeout = null;
    }
    return super.remove();
  }

  die() {
    if (this.spawnTimeout) {
      clearTimeout(this.spawnTimeout);
      this.spawnTimeout = null;
    }
    if (this.isAlive) {
      this.callback(this, "died");
    }
    return super.die();
  }
}
