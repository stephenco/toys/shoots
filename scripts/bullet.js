import Renderable from './renderable.js';
import Vector from './vector.js';
import Size from './size.js';

export default class Bullet extends Renderable {
  constructor(position, owner) {
    super(position, new Size(3, 12), new Vector().initGrid(0, -5));
    this.owner = owner;
  }

  setupPaintContext(canvas) {
    const context = canvas.getContext('2d');
    context.fillStyle = '#ff0';
    return context;
  }

  collidesWith(obj) {
    if (obj == this.owner) { return false; }
    return super.collidesWith(obj);
  }
}
