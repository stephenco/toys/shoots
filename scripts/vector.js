export default class Vector {
  constructor(...args) {
    var fnName = ((args.length > 2 && args[2]) ? 'initGrid' : 'initAngle');
    this[fnName](...args);
  }

  initAngle(angle, speed, rotation) {
    const a = angle || 0;
    const s = speed || 0;
    const r = rotation || 0;
    this.initialAngle = a;
    this.angle = a;
    this.initialSpeed = s;
    this.speed = s;
    this.initialRotation = r;
    this.rotation = r;
    return this.setGrid(Math.cos(a) * s, Math.sin(a) * s);
  }

  initGrid(x, y, rotation) {
    let r = rotation || 0;
    this.initialRotation = r;
    this.rotation = r;
    this.setGrid(x || 0, y || 0, true);
    this.initialAngle = this.angle;
    this.initialSpeed = this.speed;
    return this;
  }

  setGrid(x, y, reviseAngle) {
    this.x = x || 0;
    this.y = y || 0;
    return reviseAngle ? this.reviseAngle() : this;
  }

  reviseAngle() {

    // TODO: find angle and spsed
    this.angle = null;
    this.speed = null;

    return this;
  }
}
