# What is it?
Noisy. May emit sparks.

## Why is this?
I made this for somebody who was probably around 10 at the time.

## How is this?
```bash
npm install && npm run dev
```

Open the URL in your browser. Mouse / spacebar / arrows does stuff.

## When is this?
Now you're just being silly.
